const hamburgerlist = document.getElementsByClassName("hamburgerpart");
const hamburgercontainer = document.getElementById("hamburgercontainer");
const navul = document.getElementById("navul");
let burgcheck = true;

hamburgercontainer.addEventListener("click", hamburger);

function hamburger() {
  if (burgcheck) {
    openBurger();
  } else {
    closeBurger();
  }
}

function openBurger() {
  for (let i = 0; i < hamburgerlist.length; i++) {
    hamburgerlist[i].classList.add("active");
  }
  navul.classList.add("active");
  burgcheck = false;
}

function closeBurger() {
  for (let i = 0; i < hamburgerlist.length; i++) {
    hamburgerlist[i].classList.remove("active");
  }
  navul.classList.remove("active");
  burgcheck = true;
}
let mediaQuery = window.matchMedia("(max-width: 767px)");
function switchToMobileNav(){
if(!mediaQuery.matches){
  closeBurger()
}
}
mediaQuery.addEventListener("change",switchToMobileNav)
